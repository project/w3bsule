<?php

/**
 * @file
 * Install, update and uninstall functions for the w3bsule module.
 */

/**
 * Implementation of hook_schema().
 */
function w3bsule_schema() {
  $schema['w3bsule_group'] = array(
    'description' => 'w3bsule_group',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'size' => 'tiny',
        'not null' => TRUE,
        'description' => 'id',
        'disp-width' => '4'
      ),
      'name' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'description' => 'name',
        'default' => ''
      ),
      'description' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'description' => 'description',
        'default' => ''
      ),
    ),
    'primary key' => array('id'),
  );
  $schema['w3bsule_checklist'] = array(
    'description' => 'w3bsule_checklist',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'size' => 'tiny',
        'not null' => TRUE,
        'description' => 'id',
      ),
      'group_id' => array(
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'group_id',
      ),
      'name' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'description' => 'name',
        'default' => '',
      ),
      'module' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'description' => 'module',
        'default' => '',
      ),
      'download' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'description' => 'download',
        'default' => '',
      ),
      'enable' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'description' => 'enable',
        'default' => '',
      ),
      'configure' => array(
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'description' => 'configure',
        'default' => '',
      ),
      'order_id' => array(
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'description' => 'order id',
        'default' => 0,
      ),
      'completed' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'UNIX timestamp of when this task was completed, or 0 if the task has not yet been completed.',
      ),
      'uid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The {user}.uid of the account that completed this task.',
      ),
    ),
    'primary key' => array('id'),
  );
  return $schema;
}

/**
 * Implementation of hook_install().
 */
function w3bsule_install() {
  drupal_install_schema('w3bsule');

  $task_fields = "(id, group_id, name, module, download, enable, configure, order_id)";

/*
   // WYSWYG Settings
  db_query("INSERT INTO {w3bsule_group} VALUES (1, 'WYSIWYG Editor', 'The single most important thing you can do for on-site SEO.')");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (1, 1, 'WYSWYG Module', 'wysiwyg', 'http://drupal.org/project/wysiwyg', 'admin/build/modules', 'admin/settings/wysiwyg', 2)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (2, 1, 'imce Module', 'imce', 'http://drupal.org/project/imce', 'admin/build/modules', 'admin/settings/imce', 2)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (3, 1, 'imce wysiwyg Module', 'imce_wysiwyg', 'http://drupal.org/project/imce_wysiwyg', 'admin/build/modules', 'admin/settings/imce_wysiwyg', 2)");

  // URL tasks
  db_query("INSERT INTO {w3bsule_group} VALUES (2, 'URL paths', 'The second most important thing you can do.')");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (4, 2, 'Token (required for other modules to function)', 'token', 'http://drupal.org/project/token', 'admin/build/modules', '', 1)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (5, 2, 'Clean URLs - Activate (Usually automatic. Please double-check!)', '', '', '', 'admin/settings/clean-urls', 1)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (6, 2, 'Pathauto Module', 'pathauto', 'http://drupal.org/project/pathauto', 'admin/build/modules', 'admin/build/path/pathauto', 2)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (7, 2, 'Global Redirect Module', 'globalredirect', 'http://drupal.org/project/globalredirect', 'admin/build/modules', '', 3)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (8, 2, 'Path Redirect Module', 'path_redirect', 'http://drupal.org/project/path_redirect', 'admin/build/modules', '', 4)");
  
  // Captcha Family
  db_query("INSERT INTO {w3bsule_group} VALUES (3, 'Captcha', 'Set Captcha Family.')");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (9, 3, 'Captcha', 'captcha', 'http://drupal.org/project/captcha', 'admin/build/modules', 'admin/user/captcha/captcha', 2)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (10, 3 , 'Image Captcha', 'image_captcha', 'http://drupal.org/project/captcha', 'admin/build/modules', 'admin/user/captcha/image_captcha', 2)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (11, 3 , 'CSS Captcha', 'css_captcha', 'http://drupal.org/project/captcha_pack', 'admin/build/modules', 'admin/user/captcha/css_captcha', 2)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (12, 3 , 'Math Captcha', 'math_captcha', 'http://drupal.org/project/captcha_pack', 'admin/build/modules', 'admin/user/captcha/math_captcha', 2)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (13, 3 , 'Text Captcha', 'text_captcha', 'http://drupal.org/project/captcha_pack', 'admin/build/modules', 'admin/user/captcha/text_captcha', 2)");

  // CCK Family
  db_query("INSERT INTO {w3bsule_group} VALUES (4, 'Content Construction Kit', 'Set CCK Family.')");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (14, 4, 'Content', 'content', 'http://drupal.org/project/cck', 'admin/build/modules', '', 2)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (15, 4, 'AutoComplete Widgets', 'autocomplete_widgets', 'http://drupal.org/project/autocomplete_widgets', 'admin/build/modules', 'http://drupal.org/project/autocomplete_widgets', 2)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (16, 4, 'Field Group', 'fieldgroup', 'http://drupal.org/project/cck', 'admin/build/modules', '', 2)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (17, 4, 'Text', 'text', 'http://drupal.org/project/cck', 'admin/build/modules', '', 2)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (18, 4, 'Number', 'number', 'http://drupal.org/project/cck', 'admin/build/modules', '', 2)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (19, 4, 'Option Widgets', 'optionwidgets', 'http://drupal.org/project/cck', 'admin/build/modules', '', 2)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (20, 4, 'User Reference', 'userreference', 'http://drupal.org/project/cck', 'admin/build/modules', '', 2)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (21, 4, 'Node Reference', 'nodereference', 'http://drupal.org/project/cck', 'admin/build/modules', '', 2)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (22, 4, 'FileField', 'filefield', 'http://drupal.org/project/cck', 'admin/build/modules', '', 2)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (23, 4, 'ImageField', 'imagefield', 'http://drupal.org/project/cck', 'admin/build/modules', '', 2)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (24, 4, 'Filefield Meta', 'filefield_meta', 'http://drupal.org/project/cck', 'admin/build/modules', '', 2)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (25, 4, 'EMImage', 'emimage', 'http://drupal.org/project/emfield', 'admin/build/modules', '', 2)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (26, 4, 'EMVideo', 'emvideo', 'http://drupal.org/project/emfield', 'admin/build/modules', '', 2)");

  // Views Family
  db_query("INSERT INTO {w3bsule_group} VALUES (5, 'Views', 'Set Views Family.')");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (27, 5, 'Views', 'views', 'http://drupal.org/project/views', 'admin/build/modules', '', 2)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (28, 5, 'Views UI', 'views_ui', 'http://drupal.org/project/views', 'admin/build/modules', '', 2)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (29, 5, 'Views Bulk Operations', 'views_bulk_operations', 'http://drupal.org/project/views_bulk_operations', 'admin/build/modules', '', 2)");
  
  
    // Others
  db_query("INSERT INTO {w3bsule_group} VALUES (6, 'Others', 'Set Others Family.')");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (30, 6, 'Vertical Tabs', 'vertical_tabs', 'http://drupal.org/project/vertical_tabs', 'admin/build/modules', '', 2)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (31, 6, 'Admin Menu', 'admin_menu', 'http://drupal.org/project/admin_menu', 'admin/build/modules', '', 2)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (32, 6, 'Admin Views', 'admin_views', 'http://drupal.org/project/admin_menu', 'admin/build/modules', '', 2)");


  // Search engine account tasks
  db_query("INSERT INTO {w3bsule_group} VALUES (3, 'Create Search Engine Accounts', 'Set yourself up with the search engines.')");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (9, 3, 'Get a Google Account - You will need this for several of the steps that follow - <a href=\"https://www.google.com/accounts/NewAccount\">https://www.google.com/accounts/NewAccount</a>', '', '', '', '', 1)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (10, 3, 'Get a Yahoo Account - You will need this for steps that follow - <a href=\"http://www.yahoo.com/r/m7\">http://www.yahoo.com/r/m7</a>', '', '', '', '', 2)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (11, 3, 'Get a Windows Live ID - You will need this for steps that follow - <a href=\"https://signup.live.com/\">https://signup.live.com/</a>', '', '', '', '', 2)");

  // Visitor tracking tasks
  db_query("INSERT INTO {w3bsule_group} VALUES (4, 'Track your visitors', 'Know where your visitors are coming from and what they do while visiting your site.')");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (12, 4, 'Google Analytics Module', 'googleanalytics', 'http://drupal.org/project/google_analytics', 'admin/build/modules', 'admin/settings/googleanalytics', 1)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (13, 4, 'Sign in to your Google Analytics Account - <a href=\"http://www.google.com/analytics\">http://www.google.com/analytics</a>', '', '', '', '', 2)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (14, 4, 'Create an Analytics for your website', '', '', '', '', 3)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (15, 4, 'Paste Google Analytics code into Google Analytics Module', '', '', '', '', 4)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (16, 4, 'Authenticate your site with Google Analytics', '', '', '', '', 5)");

  // Page content tasks
  db_query("INSERT INTO {w3bsule_group} VALUES (5, 'Page content', 'Take control of your page content.')");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (17, 5, 'Meta Tags Module (AKA Nodewords)', 'nodewords', 'http://drupal.org/project/nodewords', 'admin/build/modules', 'admin/content/nodewords', 2)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (18, 5, 'Scheduler Module', 'scheduler', 'http://drupal.org/project/scheduler', 'admin/build/modules', 'admin/settings/scheduler', 3)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (19, 5, 'HTML Purifier Module', 'htmlpurifier', 'http://drupal.org/project/htmlpurifier', 'admin/build/modules', 'admin/settings/filters/1', 4)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (20, 5, '<a href=\"/sites/all/modules/htmlpurifier/INSTALL.txt\">READ THE INSTALL INSTRUCTIONS!</a> then Download HTML Purifier. You will need
    3.1.0rc1 or later. - <a href=\"http://htmlpurifier.org/\">http://htmlpurifier.org/</a>', '', '', '', '', 5)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (21, 5, 'Search 404 Module', 'search404', 'http://drupal.org/project/search404', 'admin/build/modules', 'admin/settings/search404', 5)");

  // Source code tasks
  db_query("INSERT INTO {w3bsule_group} VALUES (6, 'Clean code', 'Well written markup is very important to the search engine spiders.')");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (19, 6, 'Validate your site - <a href=\"http://validator.w3.org/\">http://validator.w3.org/</a>', '', '', '', '', 1)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (20, 6, 'Check your links - <a href=\"http://validator.w3.org/checklink\">http://validator.w3.org/checklink</a>', '', '', '', '', 2)");

  // XML sitemap tasks
  db_query("INSERT INTO {w3bsule_group} VALUES (7, 'Submit your Site to the search engines.', 'Now that you\'ve got your site ready for the search engines, tell them about it!')");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (47, 7, 'Site Verification Module', 'site_verify', 'http://drupal.org/project/site_verify', 'admin/build/modules', '', 0)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (21, 7, 'XML Sitemap Module', 'xmlsitemap', 'http://drupal.org/project/xmlsitemap', 'admin/build/modules', 'admin/settings/xmlsitemap', 1)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (46, 7, 'Site Map Module - a plain text sitemap', 'site_map', 'http://drupal.org/project/site_map', 'admin/build/modules', 'admin/settings/sitemap', 3)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (22, 7, 'Login to Google Webmaster Tools - <a href=\"http://www.google.com/webmasters/tools\">http://www.google.com/webmasters/tools</a>', '', '', '', '', 5)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (23, 7, 'Authenticate your site with Google (page 26)', '', '', '', '', 7)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (24, 7, 'Submit your XML Sitemap to Google - <a href=\"http://www.google.com/webmasters/sitemaps/\">http://www.google.com/webmasters/sitemaps/</a>', '', '', '', '', 9)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (25, 7, 'Login to Yahoo Site Explorer Account - <a href=\"https://siteexplorer.search.yahoo.com/\">https://siteexplorer.search.yahoo.com/</a>', '', '', '', '', 11)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (26, 7, 'Authenticate your site with Yahoo', '', '', '', '', 13)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (27, 7, 'Submit your XML Sitemap to Yahoo - <a href=\"https://siteexplorer.search.yahoo.com/submit\">https://siteexplorer.search.yahoo.com/submit</a>', '', '', '', '', 15)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (42, 7, 'Login to Bing - <a href=\"http://www.bing.com/webmaster/\">http://www.bing.com/webmaster/</a>', '', '', '', '', 17)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (43, 7, 'Authenticate your site with Bing', '', '', '', '', 19)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (28, 7, 'Submit your XML Sitemap to Bing - <a href=\"http://www.bing.com/webmaster/WebmasterAddSitesPage.aspx\">http://webmaster.live.com/webmaster/WebmasterAddSitesPage.aspx</a>', '', '', '', '', 21)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (29, 7, 'If appropriate, submit your company to Google Local Business Center - <a href=\"https://www.google.com/local/add/login\">https://www.google.com/local/add/login</a>', '', '', '', '', 23)");

  // Social links tasks
  db_query("INSERT INTO {w3bsule_group} VALUES (8, 'Social Media', 'Using Social news sites, blogs, etc? Consider these:')");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (30, 8, 'Digg This Module', 'diggthis', 'http://drupal.org/project/diggthis', 'admin/build/modules', 'admin/settings/diggthis', 1)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (31, 8, 'Service Links Module', 'service_links', 'http://drupal.org/project/service_links', 'admin/build/modules', 'admin/settings/service_links', 2)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (32, 8, 'Trackback Module', 'trackback', 'http://drupal.org/project/trackback', 'admin/build/modules', 'admin/settings/trackback', 3)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (48, 8, 'Activity Stream Module', 'activitystream', 'http://drupal.org/project/activitystream', 'admin/build/modules', '', 4)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (49, 8, 'Add to Any Module', 'addtoany', 'http://drupal.org/project/addtoany', 'admin/build/modules', '', 5)");

  // Spam tasks
  db_query("INSERT INTO {w3bsule_group} VALUES (9, 'Protect your site from Spam', 'If your site will get heavy use from visitors creating accounts, commenting and/or creating content then consider these. NOTE: Most sites just need Mollom. The other modules are here in case Mollom does not work for you for some reason.')");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (40, 9, 'HIGHLY RECOMMENDED: Sign up for Mollom\'s free service and get Mollom code - <a href=\"http://mollom.com/user/register\"> http://mollom.com/user/register</a>', '', '', '', '', 0)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (39, 9, 'HIGHLY RECOMMENDED: Mollom Module', 'mollom', 'http://drupal.org/project/mollom', 'admin/build/modules', 'admin/settings/mollom', 1)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (33, 9, 'Captcha Module', 'captcha', 'http://drupal.org/project/captcha', 'admin/build/modules', 'admin/user/captcha', 2)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (34, 9, 'Akismet Module', 'akismet', 'http://drupal.org/project/akismet', 'admin/build/modules', 'admin/settings/akismet', 3)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (36, 9, 'Spam Module', 'spam', 'http://drupal.org/project/spam', 'admin/build/modules', 'admin/settings/spam', 4)");

  // Geographic tasks
  db_query("INSERT INTO {w3bsule_group} VALUES (10, 'Geographic', '')");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (50, 10, 'hCard Module', 'hcard', 'http://drupal.org/project/hcard', 'admin/build/modules', '', 1)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (51, 10, 'Use the Meta Tags module to add geo meta tags to your site.', '', '', '', '', 2)");

  // Optional (but helpful) tasks
  db_query("INSERT INTO {w3bsule_group} VALUES (11, 'Optional (but helpful)', '')");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (52, 11, 'Vertical Tabs Module', 'vertical_tabs', 'http://drupal.org/project/vertical_tabs', 'admin/build/modules', '', 1)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (53, 11, 'Administration Menu', 'admin_menu', 'http://drupal.org/project/admin_menu', 'admin/build/modules', '', 2)");

  // Performance tasks
  db_query("INSERT INTO {w3bsule_group} VALUES (12, 'Performance', '')");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (54, 12, 'Turn on Drupal\'s built in caching.', '', '', '', 'admin/settings/performance/default', 1)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (55, 12, 'Boost Module', 'boost', 'http://drupal.org/project/boost', 'admin/build/modules', 'admin/settings/performance/boost', 2)");
  db_query("INSERT INTO {w3bsule_checklist} $task_fields VALUES (56, 12, 'Authcache Module', 'authcache', 'http://drupal.org/project/authcache', 'admin/build/modules', '', 3)");

*/

}

/**
 * Implementation of hook_uninstall().
 */
function w3bsule_uninstall() {
  drupal_uninstall_schema('w3bsule');
  variable_del('w3bsule_checklist_link');
  variable_del('w3bsule_checklist_thanks');
  variable_del('w3bsule_checklist_podcast');
  variable_del('w3bsule_checklist_book_references');
}
